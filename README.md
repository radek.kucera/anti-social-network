# anti-social-network
Anti-Social network je sociální síť pro asociály, která slouží jako ročíková (domácí, corona) práce čtvrtého ročníku střední školy.

**Server**
- Návrh databáze a spuštění na vzdáleném serveru ... ✔️
- Navržení DBContentu a setup EF + DI ... ✔️
- Implementace servis ... ⌛
- Vytvoření autentizace skrze Google OAuth. ✔️
- Implementace rout a endpointů serveru ... ⌛

**Klient**
- Vytvoření architektury projektu ... ✔️
- Zabezpečení přístupu a vytvoření routovacího systému ... ✔️
- Autentizace přes Google Auth ... ✔️
- Vytvoření UI komponent a vzhledu aplikace ... 🔜
- Složení výsledné aplikace ... 🔜

**Cloud**
- Založení Azure projektu (Hosting) ... ✔️
- Založení a navržení databáze Azure ... ✔️
- Nasazení projektu do Azure ... ✔️

**Dokumentace**
- Vytvoření dokumentace - 🤷

***Databáze***
![Database diagram](./dbdiagram.png)