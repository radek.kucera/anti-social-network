CREATE TABLE Users (
  `user_id` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) UNIQUE NOT NULL,
  `google_id` varchar(1024) UNIQUE NOT NULL,
  `profile_picture` varchar(1024) UNIQUE NOT NULL
);

CREATE TABLE Posts (
  `post_id` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `content` varchar(4096) NOT NULL,
  `date` datetime(3) NOT NULL,
  `owner_id` INT NOT NULL
);

CREATE TABLE Comments (
  `comment_id` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `content` varchar(512) NOT NULL,
  `date` datetime(3) NOT NULL,
  `post_id` INT NOT NULL,
  `user_id` INT NOT NULL
);


ALTER TABLE `Posts` ADD FOREIGN KEY (`owner_id`) REFERENCES Users (`user_id`);

ALTER TABLE `Comments` ADD FOREIGN KEY (`post_id`) REFERENCES Posts (`post_id`);

ALTER TABLE `Comments` ADD FOREIGN KEY (`user_id`) REFERENCES Users (`user_id`);