﻿using System;
using System.Collections.Generic;

namespace Anti_Social_Network.Models
{
    public class Posts
    {
        public Posts()
        {
            Comments = new HashSet<Comments>();
        }

        public int PostId { get; set; }
        public string Content { get; set; }
        public DateTime Date { get; set; }
        public int OwnerId { get; set; }

        public virtual Users Owner { get; set; }
        public virtual ICollection<Comments> Comments { get; set; }
    }
}