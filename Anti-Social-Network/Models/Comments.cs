﻿using System;

namespace Anti_Social_Network.Models
{
    public class Comments
    {
        public int CommentId { get; set; }
        public string Content { get; set; }
        public DateTime Date { get; set; }
        public int PostId { get; set; }
        public int UserId { get; set; }

        public virtual Posts Post { get; set; }
        public virtual Users User { get; set; }
    }
}