import React, { Component } from "react";
import { Route } from "react-router";

import { Layout } from "./containers/shared/Layout";
import Thumb from "./containers/Thumb";
import Login from "./containers/Login";
import Profile from "./containers/Profile";
import ProtectedRoute from "./components/ProtectedRoute";

import "./custom.css";

import Auth from "./services/auth";

class App extends Component {
  static displayName = App.name;

  constructor(props) {
    super(props);
    this.state = {
        auth: Auth
    }
  }

  render() {
    return (
      <Layout>
        <Route
          path="/login"
          component={() => <Login auth={this.state.auth}/>}
        />
        <ProtectedRoute exact path="/" component={Thumb} auth={this.state.auth}/>
        <ProtectedRoute path="/profile" component={Profile} auth={this.state.auth}/>
      </Layout>
    );
  }
}

export default App;