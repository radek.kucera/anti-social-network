﻿import React, { Component } from "react";
import GoogleLogin from "react-google-login";
import { Container } from "reactstrap";
import Redirect from "react-router/Redirect";

class Login extends Component {
    static displayName = Login.name;
    
    constructor(props) {
        super(props);
        this.state = {
            redirect: false
        };
        
        this.handleSignIn = this.handleSignIn.bind(this);
    }
    
    handleSignIn(response) {
        this.props.auth.signIn(response);
        this.setState({ redirect: true });
    }
   
  render() {
      if (this.state.redirect) {
          return <Redirect to="/" />
      }
      
      return (
          <Container>
            <GoogleLogin
              clientId="1028897486889-foqv230s8tgfdklnt4lh7qldke6u90qt.apps.googleusercontent.com"
              buttonText="Google"
              onSuccess={(response) => this.handleSignIn(response)}
              onFailure={() => alert("Login failed!")}
              cookiePolicy={"single_host_origin"}
            />
          </Container>
      );
  }
}

export default Login;
