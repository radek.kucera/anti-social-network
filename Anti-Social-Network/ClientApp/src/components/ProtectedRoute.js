﻿import {Redirect, Route} from "react-router";
import React from "react";

const ProtectedRoute = ({auth, component: Component, ...rest }) => {
    return (
        <Route
            {...rest}
            render={props =>
                auth.isAuthenticated === true ? (
                    <Component {...props} />
                ) : (
                    <Redirect to="/login" />
                )
            }
        />
    );
};

export default ProtectedRoute;