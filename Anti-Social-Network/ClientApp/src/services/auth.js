class Auth {
  constructor() {
    this.isAuthenticated = false;
  }

  signIn(googleResponse) {
    console.log(googleResponse.tokenId);
    
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      cache: 'no-cache',
      body: { tokenId: googleResponse.tokenId }
    };
    
    fetch("auth", options).then(res => res.json()).then(data => data.json());
    
    this.isAuthenticated = true;
  }

  signOut() {
    this.isAuthenticated = false;
  }
}

export default new Auth();
