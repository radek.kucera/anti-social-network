﻿using System.Threading.Tasks;
using Anti_Social_Network.Services.Models;
using Google.Apis.Auth;

namespace Anti_Social_Network.Types
{
    public interface IAuthService
    {
        Task<User> Authenticate(GoogleJsonWebSignature.Payload payload);
    }
}