﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Anti_Social_Network.Services.Models;
using Anti_Social_Network.Types;
using Google.Apis.Auth;

namespace Anti_Social_Network.Services
{
    public class AuthService : IAuthService
    {
        private static readonly IList<User> _users = new List<User>();

        public AuthService()
        {
            Refresh();
        }

        public async Task<User> Authenticate(GoogleJsonWebSignature.Payload payload)
        {
            await Task.Delay(1);
            return FindUserOrAdd(payload);
        }

        private User FindUserOrAdd(GoogleJsonWebSignature.Payload payload)
        {
            var u = _users.Where(x => x.email == payload.Email).FirstOrDefault();
            if (u == null)
            {
                u = new User
                {
                    id = Guid.NewGuid(),
                    name = payload.Name,
                    email = payload.Email,
                    oauthSubject = payload.Subject,
                    oauthIssuer = payload.Issuer
                };
                _users.Add(u);
            }

            return u;
        }

        private void Refresh()
        {
            if (_users.Count == 0)
            {
                _users.Add(new User {id = Guid.NewGuid(), name = "Test Person1", email = "testperson1@gmail.com"});
                _users.Add(new User {id = Guid.NewGuid(), name = "Test Person2", email = "testperson2@gmail.com"});
                _users.Add(new User {id = Guid.NewGuid(), name = "Test Person3", email = "testperson3@gmail.com"});
            }
        }
    }
}